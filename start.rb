require 'rubygems'
require 'blitz'
require 'pp'
require 'pry'


class PerfTest
#Android uses 20 at max limit there
#@url_host = "http://www.yahoo.com"

@url_host = "https://www.urbanspoon.com/api/v2/"
@endpoints = ["restaurants?limit=20", "vicinity?", "restaurants/spin?"]
@client = 'ClientId: a77ceb2511f291d73c77a2f568bda31b'
@gzip = 'Accept-Encoding: gzip, deflate'
@regions = ['california', 'australia', 'oregon', 'virginia', 'ireland']
@serverbroke, @timeout, @total = 0
  def self.sprint(lat, lon)
    url = "-v:d1 n[0,9] -v:d2 n[0,9] -v:d3 n[0,9] -v:d4 n[0,9] -T 10000 -H '#{@client}' -H '#{@gzip}' -r #{@regions.sample} #{@url_host}#{@endpoints.sample}&lat=47.7\#{d1}\#{d2}&lon=-122.3\#{d3}\#{d4}"
    pp :request => "#{url}"
    sprint = Blitz::Curl.parse(url)
    begin
      result = sprint.execute
      pp :result => result.steps[0].response.status
      #optional if needed to see restaurants
      #pp :restaurants => JSON.parse(result.steps[0].response.content)
      pp :duration => result.duration
      @total += 1
    rescue Blitz::Curl::Error::Timeout
      pp "Response timed out"
      @timeout += 1
    rescue RestClient::ServerBrokeConnection
      pp "Server Broke connection"
      @serverbroke += 1
    end
  end
  def self.rush(lat, lon)
    #url = "-p 2-1000:60 -v:ep list[#{@endpoints.join(',')}] -v:d1 n[0,9] -v:d2 n[0,9] -v:d3 n[0,9] -v:d4 n[0,9] -T 10000 -H '#{@client}' -H '#{@gzip}' -r #{@regions.sample} #{@url_host}\#{ep}&lat=47.7\#{d1}\#{d2}&lon=-122.3\#{d3}\#{d4}"
    url = "-p 2-10:60 -v:d1 n[0,9] -v:d2 n[0,9] -v:d3 n[0,9] -v:d4 n[0,9] -T 10000 -H '#{@client}' -H '#{@gzip}' -r ireland http://www.urbanspoon.com/api/isearch?&l=47.6\#{d1}\#{d2}%2C-122.3\#{d3}\#{d4}&u=perf"
    pp :request => "#{url}"
    rush = Blitz::Curl.parse(url)
    result = rush.execute
  end
end

# Create a loop for all the lat, lon in cities and then invoke sprint on it
cities_lat_lon = IO.readlines('city_lat_lon.xls')
# cities_lat_lon.each do |city|
#   id, lat,lon = city.split(' ')
#   sleep(6)
#   PerfTest.sprint(lat, lon)
# end
id, lat, lon = cities_lat_lon[0].split(' ') 
PerfTest.rush(lat, lon)

pp "Total " + PerfTest.total
pp "Total timeouts " + PerfTest.timeout
pp "Total serverbroke " + PerfTest.serverbroke

